let express = require('express');
let router = express.Router();
let config = require('config');
let fs = require('fs');
let request = require('request');
let needle = require('needle');
let phantomjs = require('phantom');
let $ = require("jquery");
let promise = require('q');
let system = require('system');
let excelbuilder = require('msexcel-builder');

let dirName = '';

let pageInst;

let id = 2500;

let subItemId = 150000;

let razd_1 = [];
let razd_2 = [];

let razd_1_id = 3000;
let razd_2_id = 30000;

let siteUrl = 'http://www.sunerzha.com';
let siteCategories = [
    'http://www.sunerzha.com/productions/water/index.php',
    'http://www.sunerzha.com/productions/electric/index.php'
]
let itemElecLinks = [];
let itemWaterLinks = [];
let items = [];


const initParse = async() => {
    console.log('initParse');
    let date = new Date();
    dirName = `sunerzha - ${date.getDate()}.${date.getMonth()}.${date.getFullYear()} - ${date.getHours()}.${date.getMinutes()}.${date.getSeconds()}`;
    await fs.mkdirSync(dirName);
    console.log('init done');
    await parseCatalog(siteCategories[0], itemWaterLinks);
    await parseCatalog(siteCategories[1], itemElecLinks);
    let classic = {
        imgLink: 'http://www.sunerzha.com/upload/iblock/c2e/p-obr-320wz450-2.jpg',
        link: 'http://www.sunerzha.com/productions/classic/products.php?IB=9&IS=113&IM=R'
    };
    itemWaterLinks.push(classic);
    await parseItems(itemWaterLinks, {
        id: 3000,
        name: 'Водяные'
    });
    await parseItems(itemElecLinks, {id: 3001, name: 'Электрические'});
    // await parseItems(['http://www.sunerzha.com/productions/classic/products.php?IB=9&IS=113&IM=R'], {id: 3002, name: 'Классические'});
    await parseSubitems(items);
    saveJson(items, 'items');
    saveXlsx();
};

const parseCatalog = async(link, arr) => {
    // for (let link of siteCategories) {
    // for (let link of [siteCategories[1]]) {
    console.log(`Start parsePage ${link}`);

    const instance = await phantomjs.create();
    const page = await instance.createPage();

    const status = await page.open(link);
    // page.injectJs('../jquery.min.js');

    await page.injectJs('jquery.min8.js');
    console.log('jQuery');

    let results = await page.evaluate(function(siteUrl) {
        var productCount = 0;
        var arr = [];
        $('table.productPreview3').each(function(id) {
            productCount++;
            var tmp = {};
            var link = siteUrl + $('table.productPreview3').eq(id).children('tbody').children('tr').eq(0).children('td').children('a').attr('href');
            var imgLink = siteUrl +
                $('table.productPreview3').eq(id).children('tbody').children('tr').eq(0).children('td').children('a').children('img').attr('src');
            tmp.link = link;
            tmp.imgLink = imgLink;
            arr.push(tmp);
        });
        return {
            arr: arr,
            productCount: productCount
        };
    }, siteUrl);
    console.log(results.arr);
    console.log('Количество предметов на странице: ' + results.productCount);
    arr.push.apply(arr, results.arr);
    console.log(`Finish parse ${link.link}`);
    await instance.exit();

    return true;
};

const parseItems = async(arr, razd) => {
    // for (let link of siteCategories) {
    // for (let link of [arr[0]]) {
        for (let link of arr) {
        console.log(`Start parseItem ${link.link}`);
        console.log(link)

        const instance = await phantomjs.create();
        const page = await instance.createPage();

        const status = await page.open(link.link);
        // page.injectJs('../jquery.min.js');

        await page.injectJs('jquery.min8.js');
        console.log('jQuery');

        let item = {};
        item.id = id;
        id++;
        item.razd = {
            name: razd.name,
            id: razd.id,
        }
        item.url = link.link;
        item.img = `img_${item.id}.png`
        await download(`${link.imgLink}`, `./${dirName}/${item.img}`, function() {});
        let name = '';
        name = await page.evaluate(function() {
            return jQuery('td.nameProduct').text();
        })
        item.name = name;

        let description = '';
        description = await page.evaluate(function() {
            return jQuery('td.textProduct').children('div').text();
        })
        item.description = description;

        item.subitems = [];

        let results = await page.evaluate(function(siteUrl) {
            var productCount = 0;
            var arr = [];
            $('table.productPreview4').each(function(id) {
                productCount++;
                var link = siteUrl + $('table.productPreview4').eq(id).children('tbody').children('tr').eq(0).children('td').children('a').attr('href');
                arr.push(link);
            });
            return {
                arr: arr,
                productCount: productCount
            };
        }, siteUrl);

        if (results.productCount == 0) {
            results = await page.evaluate(function(siteUrl) {
                var productCount = 0;
                var arr = [];
                $('table.productPreview5').each(function(id) {
                    productCount++;
                    var link = siteUrl + $('table.productPreview5').eq(id).children('tbody').children('tr').eq(0).children('td').children('a').attr('href');
                    arr.push(link);
                });
                return {
                    arr: arr,
                    productCount: productCount
                };
            }, siteUrl);
        }
        item.subitemsLinks = results.arr;
        console.log(results.arr);
        console.log('Количество сабитемов на странице: ' + results.productCount);
        // items.push.apply(items, results.arr);
        items.push(item);
        console.log(`Finish parse ${link.link}`);
        await instance.exit();
    }

    return true;
};


const parseSubitems = async(arr) => {
    // for (let link of siteCategories) {
    // for (let item of [arr[0]]) {
        for (let item of arr) {
        console.log(`Start parseItem ${item.url}`);

        for (let subitemLink of item.subitemsLinks) {
            console.log(`Start parseSubitem ${subitemLink}`);


            const instance = await phantomjs.create();
            const page = await instance.createPage();

            const status = await page.open(subitemLink);
            // page.injectJs('../jquery.min.js');

            await page.injectJs('jquery.min8.js');
            console.log('jQuery');

            let subitem = {};
            let name = '';
            name = await page.evaluate(function() {
                return jQuery('td.nameProduct').text();
            })
            subitem.name = name;

            let char = '';
            char = await page.evaluate(function() {
                var string = '';
                var strTable = jQuery('table.propTable').children('tbody').children('tr');
                strTable.each(function(id) {
                    string += strTable.eq(id).children('td.productPropDetail').text() + ': ';
                    string += strTable.eq(id).children('td.productPropValue').text() + '\n';
                })
                return string;
            })
            subitem.char = char;


            let offers = [];
            results = await page.evaluate(function() {
                var mainTable = jQuery('td.productPropArticle').parent().parent();
                var offersCount = 0;
                var offers = [];
                mainTable.children('tr').each(function(id) {
                    if (offersCount == 0) {
                        offersCount += 1;
                        return true;
                    }
                    offersCount += 1;
                    var currentOffer = mainTable.children('tr').eq(id);
                    var offer = {};
                    // Защита от
                    if (currentOffer.children('td.productPropArticle').length) {
                        if (currentOffer.children('td.productPropArticle').hasClass('articlePolymer')) {
                            offer.protection = 'Да';
                        } else {
                            offer.protection = 'Нет'
                        };
                    } else {
                        offer.protection = '';
                    }
                    // Покрытие
                    if (currentOffer.children('td.productPropCover').length) {
                        offer.cover = currentOffer.children('td.productPropCover').text();
                    } else {
                        offer.cover = '';
                    }
                    // Соединение
                    if (currentOffer.children('td.productPropConnection').length) {
                        offer.connection = currentOffer.children('td.productPropConnection').text()
                    } else {
                        offer.connection = '';
                    }
                    // Цена
                    if (currentOffer.children('td.productPropPrice').length) {
                        offer.price = currentOffer.children('td.productPropPrice').text()
                    } else {
                        offer.price = '';
                    }

                    // Перемычка
                    if (currentOffer.children('td.productPropJumper').length) {
                        offer.jumper = currentOffer.children('td.productPropJumper').text()
                    } else {
                        offer.jumper = '';
                    }

                    offers.push(offer);
                });
                return {
                    arr: offers,
                    count: offersCount
                }
            });

            let imgLink = '';
            let imgName = '';
            imgLink = await page.evaluate(function(name) {
                return jQuery('a[title="' + name + '"]').eq(0).attr('href');
            }, subitem.name)
            console.log(imgLink);
            //
            imgName = `img_${item.id}_${subItemId}.png`;
            await download(`${siteUrl}${imgLink}`, `./${dirName}/${imgName}`, function() {});
            subitem.img = imgName;
            subitem.offers = results.arr;
            for (let sub of subitem.offers) {
                sub.iddd = subItemId;
                subItemId++;
                // Получить размер
                let tmp = subitem.name.split(' ');
                sub.size = tmp[tmp.length - 1];
            }


            console.log(`Count: ${results.count}`);
            item.subitems.push(subitem);
            console.log(`Finish parse ${subitemLink}`);
            await instance.exit();
        }
    }

    return true;
};
router.get('/',
    function(req, res, next) {
        initParse();
        res.json('Начинаем парсить sunerzha.com');
    });



const download = function(uri, filename, callback) {
    request.head(uri, function(err, res, body) {
        request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
    });
};

const saveJson = async(arr, name) => {
    const json = JSON.stringify(arr);
    await fs.writeFile("./" + dirName + "/" + name + ".json", json, 'utf8', function(err) {
        if (err) {
            return console.log(err);
        }
        console.log(`JSON ${name} сохранен`);
    });
    return true;
};


const saveXlsx = async() => {
    let workbookImport = await excelbuilder.createWorkbook('./' + dirName + '/', 'import.xlsx');
    let workbookOffers = await excelbuilder.createWorkbook('./' + dirName + '/', 'offers.xlsx');

    let rowCountImport = items.length + 1;
    let rowCountOffers = 1;
    for (let item of items) {
        for (let subitem of item.subitems) {
            rowCountOffers += subitem.offers.length;
        }
    }

    let importSheet = await workbookImport.createSheet('Товары', 15, rowCountImport);
    let offersSheet = await workbookOffers.createSheet('Товарные предложения', 15, rowCountOffers);

    importSheet.width(1, 10);
    importSheet.width(2, 30);
    importSheet.width(3, 40);
    importSheet.width(4, 40);
    importSheet.width(5, 40);
    importSheet.width(6, 50);
    importSheet.width(7, 50);
    importSheet.width(8, 50);

    importSheet.set(1, 1, "id");
    importSheet.set(2, 1, "id_razd");
    importSheet.set(3, 1, "razd");
    importSheet.set(4, 1, "name");
    importSheet.set(5, 1, "description");
    importSheet.set(6, 1, "img");

    let rowCounter = 2;
    for (let item of items) {
        importSheet.height(rowCounter, 30);

        importSheet.set(1, rowCounter, item.id);
        importSheet.set(2, rowCounter, item.razd.id);
        importSheet.set(3, rowCounter, item.razd.name);
        importSheet.set(4, rowCounter, item.name);
        importSheet.set(5, rowCounter, item.description);
        importSheet.set(6, rowCounter, item.img);

        rowCounter++;
    }


    await workbookImport.save(function(ok) {
        if (!ok)
            workbookImport.cancel();
    });

    offersSheet.width(1, 10);
    offersSheet.width(2, 30);
    offersSheet.width(3, 40);
    offersSheet.width(4, 40);
    offersSheet.width(5, 40);
    offersSheet.width(6, 50);
    offersSheet.width(7, 50);
    offersSheet.width(8, 15);
    offersSheet.width(9, 15);

    offersSheet.set(1, 1, "id");
    offersSheet.set(2, 1, "price");
    offersSheet.set(3, 1, "razmer");
    offersSheet.set(4, 1, "char");
    offersSheet.set(5, 1, "connection");
    offersSheet.set(6, 1, "cover");
    offersSheet.set(7, 1, "jumper");
    offersSheet.set(8, 1, "protection");
    offersSheet.set(9, 1, "name");
    offersSheet.set(10, 1, "img");
    offersSheet.set(11, 1, "val");
    offersSheet.set(12, 1, "Iddd");

    rowCounter = 2;

    for (let item of items) {
        for (let subitem of item.subitems) {
            for (let offer of subitem.offers) {

                offersSheet.height(rowCounter, 30);

                offersSheet.set(1, rowCounter, item.id);
                offersSheet.set(2, rowCounter, offer.price);
                offersSheet.set(3, rowCounter, offer.size);
                offersSheet.set(4, rowCounter, subitem.char);
                offersSheet.set(5, rowCounter, offer.connection);
                offersSheet.set(6, rowCounter, offer.cover);
                offersSheet.set(7, rowCounter, offer.jumper);
                offersSheet.set(8, rowCounter, offer.protection);
                offersSheet.set(9, rowCounter, subitem.name);
                offersSheet.set(10, rowCounter, subitem.img);
                offersSheet.set(11, rowCounter, 'val');
                offersSheet.set(12, rowCounter, offer.iddd);

                rowCounter++;
            }
        }
    }

    await workbookOffers.save(function(ok) {
        if (!ok)
            workbookOffers.cancel();
    });

    console.log(`
                  _______________________________________________________
                  |                                                      |
             /    |                  ФУРА                                |
            /---, |                   С                                  |
       -----# ==| |                 СУШИЛКАМИ                            |
       | :) # ==| |                                                      |
  -----'----#   | |______________________________________________________|
  |)___()  '#   |______====____   \___________________________________|
 [_/,-,\"--"------ //,-,  ,-,\\\   |/             //,-,  ,-,  ,-,\\ __#
   ( 0 )|===******||( 0 )( 0 )||-  o              '( 0 )( 0 )( 0 )||
----'-'--------------'-'--'-'-----------------------'-'--'-'--'-'--------------
        `);

    return true;
};


module.exports = router;
