let express = require('express');
let router = express.Router();
let config = require('config');
let fs = require('fs');
let request = require('request');
let needle = require('needle');
let phantomjs = require('phantom');
let $ = require("jquery");
let promise = require('q');
let system = require('system');
let excelbuilder = require('msexcel-builder');

let id = 500;
let subItemId = 2000;
let noSubItems = 0;

let colors = [{
        name: 'Хром',
        cost: 1,
        ename: 'hrom'
    },
    {
        name: 'Аккорд',
        cost: 2,
        ename: 'akkord'
    },
    {
        name: 'Белый',
        cost: 2,
        ename: 'belyj'
    },
    {
        name: 'Бронза',
        cost: 2,
        ename: 'bronza'
    },
    {
        name: 'Золото',
        cost: 2,
        ename: 'zoloto'
    },
    {
        name: 'Латунь',
        cost: 2,
        ename: 'latun'
    },
    {
        name: 'Черный',
        cost: 2,
        ename: 'chernyj'
    },
];

let protections = [{
        name: 'Стандарт',
        cost: 0
    },
    {
        name: 'Полимер',
        cost: 2500
    }
]

let razd_1 = [];
let razd_2 = [];
let razd_3 = [];

let razd_1_id = 1000;
let razd_2_id = 10000;
let razd_3_id = 100000;

let siteUrl = 'http://www.trugor.ru';
let waterDrierCategory = 'http://trugor.ru/katalog/vodyanye-polotentsesushiteli/';
let electroDrierCategory = 'http://trugor.ru/katalog/elektricheskie-polotentsesushiteli/';

let waterPages = [];
let electoPages = [];

let waterItemLinks = [];
let electroItemLinks = [];

let waterItems = [];
let electoItems = [];
let designItems = [];

let dirName = '';

let pageInst;


const download = function(uri, filename, callback) {
    return new Promise(function(resolve, reject) {
        request.head(uri, function(err, res, body) {
            request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
            resolve(true);
        });
    });
};


const initParse = async() => {
    console.log('initParse');
    let date = new Date();
    dirName = `trugor - ${date.getDate()}.${date.getMonth()}.${date.getFullYear()} - ${date.getHours()}.${date.getMinutes()}.${date.getSeconds()}`;
    console.log('createFolder');
    await fs.mkdirSync(dirName);

    console.log('Start parse water driers');
    const instanceW = await phantomjs.create(['--load-images=no']);
    const pageW = await instanceW.createPage();
    const statusW = await pageW.open(waterDrierCategory);
    if (statusW === 'success') {
        // page.injectJs('../jquery.min.js');
        // console.log('Загрузили $');
        console.log('Начинаем парсить пагинатор');
        await parseWaterPagination(pageW, waterDrierCategory)
    };
    await instanceW.exit();
    console.log('End parse water driers');


     //Электросушилки
    console.log('Start parse electro driers');
    const instanceE = await phantomjs.create(['--load-images=no']);
    const pageE = await instanceE.createPage();
    const statusE = await pageE.open(electroDrierCategory);
    if (statusE === 'success') {
        // page.injectJs('../jquery.min.js');
        // console.log('Загрузили $');
        console.log('Начинаем парсить пагинатор');
        await parseElectroPagination(pageE, electroDrierCategory)
    };
    await instanceE.exit();



    // await parseWaterItem([waterItemLinks[0]]);

    //debug
    // let debLink = 'http://www.trugor.ru/products/vodyanoj-lc6pm/';
    await parseCatalog();
    // await parseWaterItem(['http://www.trugor.ru/products/vodyanoj-lc3pm/']);
    // await parseWaterItem(waterItemLinks.slice(0, 12));
    await parseWaterItem(waterItemLinks);
    // await parseWaterItem([waterItemLinks[48]]);
    // saveJson(waterItems, 'waterItems');
    await parseWaterItem(electroItemLinks);
    // await parseWaterItem([electroItemLinks[0]]);
    saveJson(razd_1, 'razd_1');
    saveJson(razd_2, 'razd_2');
    saveJson(razd_3, 'razd_3');
    saveImportWaterXlsx();

    console.log('Объектов без сабитемов: ' + noSubItems);
};


const parseWaterPagination = async(page, category) => {
    console.log(`Парсим пагинацию на странице ${category}`);
    let pagesLinks = await page.evaluate(function(category) {
        var arr = [];
        var lastPage = +jQuery('ul.page-numbers').children('li').eq('-2').prop('textContent');
        for (var i = 1; i <= lastPage; i++) {
            arr.push(category + 'page/' + i);
        };
        return arr;
    }, category);
    console.log(`Закончили парсить пагинацию на странице ${category}`);
    waterPages = pagesLinks;
    console.log(waterPages);
    return true;
};

const parseElectroPagination = async(page, category) => {
    console.log(`Парсим пагинацию на странице ${category}`);
    let pagesLinks = await page.evaluate(function(category) {
        var arr = [];
        var lastPage = +jQuery('ul.page-numbers').children('li').eq('-2').prop('textContent');
        for (var i = 1; i <= lastPage; i++) {
            arr.push(category + 'page/' + i);
        };
        return arr;
    }, category);
    console.log(`Закончили парсить пагинацию на странице ${category}`);
    electroPages = pagesLinks;
    console.log(electroPages);
    return true;
};


const parseCatalog = async() => {
    console.log('Начинаем парсить каталог водяных сушилок');
    for (let link of waterPages) {
        await parsePage(link, waterItemLinks);
    }
    // await parsePage(waterPages[0], waterItemLinks);
    console.log(waterItemLinks);
    console.log(waterItemLinks.length);
    console.log('Закончили парсить каталог водяных сушилок');


    console.log('Начинаем парсить каталог электрических сушилок');
    for (let link of electroPages) {
        await parsePage(link, electroItemLinks);
    }
    // await parsePage(waterPages[0], waterItemLinks);
    console.log(electroItemLinks);
    console.log(electroItemLinks.length);
    console.log('Закончили парсить каталог электрических сушилок');
    return true;
};


const parsePage = async(link, itemArray) => {
    console.log(`Start parsePage ${link}`);

    const instance = await phantomjs.create();
    const page = await instance.createPage();

    const status = await page.open(link);
    console.log(status);

    let results = await page.evaluate(function(siteUrl) {
        var ctl = 'div.asdf';
        var ctlCount = 0;
        var productCount = 0;
        var arr = [];
        jQuery(ctl).children('li').each(function(id) {
            productCount++;
            jQuery(ctl).children('li').eq(id).children('a').eq(0).attr('href');
            arr.push(siteUrl +
                jQuery(ctl).children('li').eq(id).children('a').eq(0).attr('href')
            );
        });
        return {
            arr: arr,
            ctlCount: ctlCount,
            productCount: productCount
        };
    }, siteUrl);
    console.log(results.arr);
    itemArray.push.apply(itemArray, results.arr);
    // itemArray = itemArray.concat(results.arr);
    console.log('Количество предметов на странице: ' + results.productCount);
    await instance.exit();
};


const parseWaterItem = async(arr) => {
    for (let itemLink of arr) {

        let instance = await phantomjs.create(['--load-images=no']);
        let page = await instance.createPage();

        let status = await page.open(itemLink);
        console.log('Зашли на страницу: ' + itemLink);

        let item = {};
        item.id = Number(id);
        console.log('Записали ID: ' + item.id);

        item.url = itemLink;

        let crumbs = await page.evaluate(function() {
            var result = {};
            var arr = [];
            result.amount = jQuery('#crumbs ul').children('li').length;
            jQuery('#crumbs ul').children('li').each(function(id) {
                var tmp = {};
                tmp.name =
                    jQuery('#crumbs ul').children('li').eq(id)
                    .children('span').children('a').children('span').text();
                tmp.url =
                    jQuery('#crumbs ul').children('li').eq(id)
                    .children('span').children('a').attr('href');
                arr.push(tmp);
            });
            result.arr = arr;
            return result;
        });

        console.log(crumbs);

        let _razd_1 = {};
        let _razd_2 = {};
        let _razd_3 = {};

        switch (crumbs.amount) {
            case 5:
                item.name = crumbs.arr[4].name;
                _razd_1 = crumbs.arr[1];
                _razd_2 = crumbs.arr[2];
                _razd_3 = crumbs.arr[3];

                // Главный раздел
                _razd_1_b = false;
                for (let razd of razd_1) {
                    if (razd.name == _razd_1.name) {
                        item.razd_1 = razd;
                        _razd_1_b = true;
                        break;
                    }
                };

                if (!_razd_1_b) {
                    razd_1.push({
                        id: razd_1_id,
                        name: _razd_1.name,
                        url: _razd_1.url,
                    });
                    razd_1_id++;
                    item.razd_1 = razd_1[razd_1.length - 1];
                };

                // Подкаталог 1 уровня
                _razd_2_b = false;
                for (let razd of razd_2) {
                    if (razd.name == _razd_2.name) {
                        item.razd_2 = razd;
                        _razd_2_b = true;
                        break;
                    }
                };

                if (!_razd_2_b) {
                    razd_2.push({
                        id: razd_2_id,
                        name: _razd_2.name,
                        url: _razd_2.url,
                    });
                    razd_2_id++;
                    item.razd_2 = razd_2[razd_2.length - 1];
                };

                // Подкаталог 2 уровня
                _razd_3_b = false;
                for (let razd of razd_3) {
                    if (razd.name == _razd_3.name) {
                        item.razd_3 = razd;
                        _razd_3_b = true;
                        break;
                    }
                };

                if (!_razd_3_b) {
                    razd_3.push({
                        id: razd_3_id,
                        name: _razd_3.name,
                        url: _razd_3.url,
                    });
                    razd_3_id++;
                    item.razd_3 = razd_3[razd_3.length - 1];
                };
                break;
            case 4:
                item.name = crumbs.arr[3].name;
                _razd_1 = crumbs.arr[1];
                _razd_2 = crumbs.arr[2];

                // Главный раздел
                _razd_1_b = false;
                for (let razd of razd_1) {
                    if (razd.name == _razd_1.name) {
                        item.razd_1 = razd;
                        _razd_1_b = true;
                        break;
                    }
                };

                if (!_razd_1_b) {
                    razd_1.push({
                        id: razd_1_id,
                        name: _razd_1.name,
                        url: _razd_1.url,
                    });
                    razd_1_id++;
                    item.razd_1 = razd_1[razd_1.length - 1];
                };

                // Подкаталог 1 уровня
                _razd_2_b = false;
                for (let razd of razd_2) {
                    if (razd.name == _razd_2.name) {
                        item.razd_2 = razd;
                        _razd_2_b = true;
                        break;
                    }
                };

                if (!_razd_2_b) {
                    razd_2.push({
                        id: razd_2_id,
                        name: _razd_2.name,
                        url: _razd_2.url,
                    });
                    razd_2_id++;
                    item.razd_2 = razd_2[razd_2.length - 1];
                };

                item.razd_3 = _razd_3;

                // Подкаталог 2 уровня
                item.razd_3.id = '';
                item.razd_3.name = '';
                item.razd_3.url = '';
                break;
            case 3:
                item.name = crumbs.arr[2].name;
                _razd_1 = crumbs.arr[1];
                // Главный раздел
                _razd_1_b = false;
                for (let razd of razd_1) {
                    if (razd.name == _razd_1.name) {
                        item.razd_1 = razd;
                        _razd_1_b = true;
                        break;
                    }
                };

                if (!_razd_1_b) {
                    razd_1.push({
                        id: razd_1_id,
                        name: _razd_1.name,
                        url: _razd_1.url,
                    });
                    razd_1_id++;
                    item.razd_1 = razd_1[razd_1.length - 1];
                };

                item.razd_2 = _razd_2;
                item.razd_3 = _razd_3;

                // Подкаталог 2 уровня
                item.razd_2.id = '';
                item.razd_2.name = '';
                item.razd_2.url = '';
                // Подкаталог 2 уровня
                item.razd_3.id = '';
                item.razd_3.name = '';
                item.razd_3.url = '';
                break;

        }

        if (item.razd_1.url == '/katalog/radiatory-dizaynerskie-el/') {
            // items.push(item);
            designItems.push(item);
            // console.log(item);
            id++;
            await instance.exit();
            continue;

        }

        let description = await page.evaluate(function() {
            var descString = '';
            jQuery('#tab-description table.shop_attributes tbody').children('tr').each(function(id) {
                descString += jQuery('#tab-description table.shop_attributes tbody').children('tr').eq(id).children('th').text() + ': ';
                descString += jQuery('#tab-description table.shop_attributes tbody').children('tr').eq(id).children('td').children('p').text() + '\n';
            });
            return descString;
        });

        console.log(description);

        item.description = description;

        item.subitems = [];

        let subitems = await page.evaluate(function() {
            return JSON.parse(jQuery('form.variations_form.cart').attr('data-product_variations'));
        })

        console.log('Записали сабитемы');
        // console.log(subitems);

        // item.subi = subitems;

        if (!subitems) {
            subitems = [];
            console.log('Сабитемов нет');
            console.log('Пытаемся сделать пост')

            console.log('Забираем product_id');
            let product_id = await page.evaluate(function() {
                return jQuery('div.product').attr('id');
            })
            console.log(product_id);

            console.log('Забираем размеры');
            let sizes = await page.evaluate(function() {
                var arr = [];
                jQuery('select#pa_vysota-i-shirina').children('option').each(function(id) {
                    arr.push(jQuery('select#pa_vysota-i-shirina').children('option').eq(id).val());
                })
                return arr;
            })
            console.log(sizes);

            console.log('Забираем диаметры');
            let podkl = await page.evaluate(function() {
                var arr = [];
                jQuery('select#pa_dopolnitelno').children('option').each(function(id) {
                    arr.push(jQuery('select#pa_dopolnitelno').children('option').eq(id).val());
                })
                return arr;
            })
            console.log(podkl);


            let zashita = await page.evaluate(function() {
                var arr = [];
                if (jQuery('select#pa_zashita').length) {
                    jQuery('select#pa_zashita').children('option').each(function(id) {
                        arr.push(jQuery('select#pa_zashita').children('option').eq(id).val());
                    });
                    return arr;
                } else {
                    return false;
                }
            });

            console.log('Защита:');
            console.log(zashita);

            let tsveta = await page.evaluate(function() {
                var arr = [];
                if (jQuery('select#pa_tsvet').length) {
                    jQuery('select#pa_tsvet').children('option').each(function(id) {
                        arr.push(jQuery('select#pa_tsvet').children('option').eq(id).val());
                    });
                    return arr;
                } else {
                    return false;
                }
            });
            console.log('Цвета:');
            console.log(tsveta);

            let furniture = await page.evaluate(function() {
                var arr = [];
                if (jQuery('select#pa_furnitura').length) {
                    jQuery('select#pa_furnitura').children('option').each(function(id) {
                        arr.push(jQuery('select#pa_furnitura').children('option').eq(id).val());
                    });
                    return arr;
                } else {
                    return false;
                }
            });

            console.log('Фурнитура');
            console.log(furniture);



            let mesto = await page.evaluate(function() {
                var arr = [];
                if (jQuery('select#pa_mesto-podklyucheniya').length) {
                    jQuery('select#pa_mesto-podklyucheniya').children('option').each(function(id) {
                        arr.push(jQuery('select#pa_mesto-podklyucheniya').children('option').eq(id).val());
                    });
                    return arr;
                } else {
                    return false;
                }
            });

            console.log('Место подключения');
            console.log(mesto);


            let ajaxLink = item.url + '?wc-ajax=get_variation';
            console.log(ajaxLink);

            for (let podk of podkl) {
                // for (let podk of [podkl[0]]) {
                console.log('Зашли в подключения: ' + podk);
                for (let size of sizes) {
                    console.log('Зашли в размеры: ' + size);
                    let tsvet = 'hrom';
                    let zash = 'standart';
                    console.log('Зашли в цвета: ' + tsvet);
                    if (zashita) {
                        if (furniture) {
                            for (let fur of furniture) {
                                console.log('Зашли в защиту: ' + zash);
                                if (mesto) {
                                    for (let mest of mesto) {
                                        let json = {};
                                        json['attribute_pa_dopolnitelno'] = podk;
                                        json['attribute_pa_vysota-i-shirina'] = size;
                                        json['product_id'] = product_id;
                                        json['attribute_pa_zashita'] = zash;
                                        json['attribute_pa_tsvet'] = tsvet;
                                        json['attribute_pa_furnitura'] = fur;
                                        json['attribute_pa_mesto-podklyucheniya'] = mest;
                                        console.log(json);
                                        let result = await doRequest(ajaxLink, json);
                                        if (result) {
                                            subitems.push(result);
                                            console.log(result);
                                        }
                                    }
                                } else {
                                    let json = {};
                                    json['attribute_pa_dopolnitelno'] = podk;
                                    json['attribute_pa_vysota-i-shirina'] = size;
                                    json['product_id'] = product_id;
                                    json['attribute_pa_zashita'] = zash;
                                    json['attribute_pa_tsvet'] = tsvet;
                                    json['attribute_pa_furnitura'] = fur;
                                    console.log(json);
                                    let result = await doRequest(ajaxLink, json);
                                    if (result) {
                                        subitems.push(result);
                                        console.log(result);
                                    }
                                }
                            }
                        } else {
                            console.log('Зашли в защиту: ' + zash);
                            if (mesto) {
                                for (let mest of mesto) {
                                    let json = {};
                                    json['attribute_pa_dopolnitelno'] = podk;
                                    json['attribute_pa_vysota-i-shirina'] = size;
                                    json['product_id'] = product_id;
                                    json['attribute_pa_zashita'] = zash;
                                    json['attribute_pa_mesto-podklyucheniya'] = mest;
                                    json['attribute_pa_tsvet'] = tsvet;
                                    console.log(json);
                                    let result = await doRequest(ajaxLink, json);
                                    if (result) {
                                        subitems.push(result);
                                        console.log(result);
                                    }
                                }
                            } else {
                                let json = {};
                                json['attribute_pa_dopolnitelno'] = podk;
                                json['attribute_pa_vysota-i-shirina'] = size;
                                json['product_id'] = product_id;
                                json['attribute_pa_zashita'] = zash;
                                json['attribute_pa_tsvet'] = tsvet;
                                console.log(json);
                                let result = await doRequest(ajaxLink, json);
                                if (result) {
                                    subitems.push(result);
                                    console.log(result);
                                }
                            }
                        }
                    } else {
                        if (furniture) {
                            for (let fur of furniture) {
                                console.log('Без защиты');
                                if (mesto) {
                                    for (let mest of mesto) {
                                        let json = {};
                                        json['attribute_pa_dopolnitelno'] = podk;
                                        json['attribute_pa_vysota-i-shirina'] = size;
                                        json['product_id'] = product_id;
                                        json['attribute_pa_tsvet'] = tsvet;
                                        json['attribute_pa_furnitura'] = fur;
                                        json['attribute_pa_mesto-podklyucheniya'] = mest;
                                        console.log(json);
                                        let result = await doRequest(ajaxLink, json);
                                        if (result) {
                                            subitems.push(result);
                                            console.log(result);
                                        }
                                    }
                                } else {
                                    let json = {};
                                    json['attribute_pa_dopolnitelno'] = podk;
                                    json['attribute_pa_vysota-i-shirina'] = size;
                                    json['product_id'] = product_id;
                                    json['attribute_pa_tsvet'] = tsvet;
                                    json['attribute_pa_furnitura'] = fur;
                                    console.log(json);
                                    let result = await doRequest(ajaxLink, json);
                                    if (result) {
                                        subitems.push(result);
                                        console.log(result);
                                    }
                                }
                            }
                        } else {
                            if (mesto) {
                                for (let mest of mesto) {
                                    console.log('Без защиты');
                                    let json = {};
                                    json['attribute_pa_dopolnitelno'] = podk;
                                    json['attribute_pa_vysota-i-shirina'] = size;
                                    json['product_id'] = product_id;
                                    json['attribute_pa_tsvet'] = tsvet;
                                    json['attribute_pa_mesto-podklyucheniya'] = mest;
                                    console.log(json);
                                    let result = await doRequest(ajaxLink, json);
                                    if (result) {
                                        subitems.push(result);
                                        console.log(result);
                                    }
                                }
                            } else {
                                console.log('Без защиты');
                                let json = {};
                                json['attribute_pa_dopolnitelno'] = podk;
                                json['attribute_pa_vysota-i-shirina'] = size;
                                json['product_id'] = product_id;
                                json['attribute_pa_tsvet'] = tsvet;
                                console.log(json);
                                let result = await doRequest(ajaxLink, json);
                                if (result) {
                                    subitems.push(result);
                                    console.log(result);
                                }
                            }
                        }
                    }
                }
            }
        }


        console.log('Ошибка?1')

        let images = [];
        console.log('Ошибка?2')

        saveJson(subitems, 'subitems_' + item.id);


        for (let sub of subitems) {
            console.log(sub);
            console.log('Зашли в сабитем')
            if (item.razd_1.url == '/katalog/radiatory-dizaynerskie-el/') {
                break;
            }
            let tmp = {};
            tmp.id = item.id;
            tmp.price = sub['display_regular_price'] || 1;
            tmp.razmer = sub['attributes']['attribute_pa_vysota-i-shirina'] || 0;
            switch (sub['attributes']['attribute_pa_dopolnitelno']) {
                case '1-dyujm':
                    tmp.podkl = '1 дюйм'
                    break;
                case '3-4-dyujma':
                    tmp.podkl = '3/4 дюйма'
                    break;
                case 'skrytoe':
                    tmp.podkl = 'Скрытое'
                    break;
                case 'obychnoe':
                    tmp.podkl = 'Шнур с вилкой'
                    break;
                case 'ten-s-termoregulirovkoj':
                    tmp.podkl = 'Тэн с терморегулировкой'
                    break;
                default:
                    tmp.podkl = '';
                    break;
            };
            tmp.name = item.name;

            if ('attribute_pa_furnitura' in sub['attributes']) {
                // Если есть фурнитура
                switch (sub['attributes']['attribute_pa_furnitura']) {
                    case 's-kranom':
                        tmp.furniture = 'С краном';
                        break;
                    case 'standart':
                        tmp.furniture = 'Стандарт';
                        break;
                    default:
                        tmp.furniture = '';
                        break;
                }
            } else {
                // Если фурнитуры нет
                tmp.furniture = '';
            }

            if ('attribute_pa_mesto-podklyucheniya' in sub['attributes']) {
                // Если есть фурнитура
                switch (sub['attributes']['attribute_pa_mesto-podklyucheniya']) {
                    case 'sleva':
                        tmp.type = 'Слева';
                        break;
                    case 'sprava':
                        tmp.type = 'Справа';
                        break;
                    default:
                        tmp.furniture = '';
                        break;
                }
            } else {
                // Тип подключения
                tmp.type = '';
            }


            // console.log('До цветов');
            console.log(tmp);
            if ('attribute_pa_tsvet' in sub['attributes']) {
                for (let color of colors) {
                    // Если есть цвета
                    console.log(color);

                    let tmpC = {};
                    for (let key in tmp) {
                        tmpC[key] = tmp[key];
                    }

                    tmpC.color = color.name;
                    tmpC.price *= color.cost;

                    if ('attribute_pa_zashita' in sub['attributes']) {
                        for (let prot of protections) {
                            // Если есть защита
                            console.log(prot);

                            let tmpCP = {};
                            for (let key in tmpC) {
                                tmpCP[key] = tmpC[key];
                            }

                            tmpCP.protection = prot.name;
                            tmpCP.price += prot.cost;
                            tmpCP.iddd = subItemId;
                            if (sub['image_src'] != '') {
                                tmpCP.img = `img_${item.id}_${tmpCP.iddd}.png`;
                                await download(sub['image_src'], `./${dirName}/${tmpCP.img}`, function() {});
                            }

                            item.subitems.push(tmpCP);
                            subItemId++;
                        }
                    } else {
                        tmpC.protection = '';
                        tmpC.iddd = subItemId;

                        if (sub['image_src'] != '') {
                            tmpC.img = `img_${item.id}_${tmpC.iddd}.png`;
                            await download(sub['image_src'], `./${dirName}/${tmpC.img}`, function() {});
                        }

                        item.subitems.push(tmpC);
                        subItemId++;
                        // Если защиты нет
                    }
                }
            } else {
                // Если нет цвета
            }
        }

        // ToDo сделать реально промисом загрузку картинок, иначе хуйня
        if (subitems[0]['image_src'] != '') {
                    item.img = `img_${item.id}.png`;
                    await download(subitems[0]['image_src'], `./${dirName}/${item.img}`, function() {});
        }





        console.log('Ошибка?3')
        saveJson(item, 'item_' + item.id);


        // items.push(item);
        waterItems.push(item);
        // console.log(item);
        id++;
        await instance.exit();
    }
};


const saveJson = async(arr, name) => {
    const json = JSON.stringify(arr);
    await fs.writeFile("./" + dirName + "/" + name + ".json", json, 'utf8', function(err) {
        if (err) {
            return console.log(err);
        }
        console.log("JSON сохранен");
    });
    return true;
};

const saveImportWaterXlsx = async() => {
    console.log('zawli!')
    let workbook = await excelbuilder.createWorkbook('./' + dirName + '/', 'importWater.xlsx');
    let workbook2 = await excelbuilder.createWorkbook('./' + dirName + '/', 'offersWater.xlsx');

    console.log('Сделали воркбуки')
    let rowCount1 = waterItems.length + 1;
    let rowCount2 = 1;
    for (let item of waterItems) {
        rowCount2 += item.subitems.length;
    }

    console.log(rowCount1);
    console.log(rowCount2);
    console.log('Посчитали счетчики')

    let sheet = await workbook.createSheet('Лист1', 15, rowCount1);
    let asdf = await workbook2.createSheet('Лист1', 15, rowCount2);

    console.log('Сделали листы')

    // Настройки ширины столбцов
    sheet.width(1, 10);
    sheet.width(2, 30);
    sheet.width(3, 40);
    sheet.width(4, 40);
    sheet.width(5, 40);
    sheet.width(6, 50);
    sheet.width(7, 50);
    sheet.width(8, 15);
    sheet.width(9, 15);
    sheet.width(10, 15);

    console.log('Столбцы 1')


    // Шапка файла
    sheet.set(1, 1, "id");
    sheet.set(2, 1, "id_razd");
    sheet.set(3, 1, "razd");
    sheet.set(4, 1, "id_razd_2");
    sheet.set(5, 1, "razd_2");
    sheet.set(6, 1, "id_razd_3");
    sheet.set(7, 1, "razd_3");
    sheet.set(8, 1, "name");
    sheet.set(9, 1, "description");
    sheet.set(10, 1, "images");

    console.log('Шапка 1')


    let rowCounter = 2;
    for (let item of waterItems) {
        console.log('Делаем первый лист')

        // console.log(item);

        sheet.height(rowCounter, 30);

        sheet.set(1, rowCounter, item.id);
        sheet.set(2, rowCounter, item.razd_1.id);
        sheet.set(3, rowCounter, item.razd_1.name);
        sheet.set(4, rowCounter, item.razd_2.id);
        sheet.set(5, rowCounter, item.razd_2.name);
        sheet.set(6, rowCounter, item.razd_3.id);
        sheet.set(7, rowCounter, item.razd_3.name);
        sheet.set(8, rowCounter, item.name);
        sheet.set(9, rowCounter, item.description);
        sheet.set(10, rowCounter, item.img);

        rowCounter++;
    }


        await workbook.save(function(ok) {
            if (!ok)
                workbook.cancel();
        });
    // Настройки ширины столбцов
    asdf.width(1, 10);
    asdf.width(2, 30);
    asdf.width(3, 40);
    asdf.width(4, 40);
    asdf.width(5, 40);
    asdf.width(6, 50);
    asdf.width(7, 50);
    asdf.width(8, 15);
    asdf.width(9, 15);
    asdf.width(10, 15);
    asdf.width(11, 15);
    console.log('Настроили ширину')
    // Шапка файла
    asdf.set(1, 1, "id");
    asdf.set(2, 1, "price");
    asdf.set(3, 1, "podkl");
    asdf.set(4, 1, "furniture");
    asdf.set(5, 1, "razmer");
    asdf.set(6, 1, "type");
    asdf.set(7, 1, "protection");
    asdf.set(8, 1, "color");
    asdf.set(9, 1, "name");
    asdf.set(10, 1, "val");
    asdf.set(11, 1, "Iddd");
    asdf.set(12, 1, "img");

    console.log('Шапка 2');
    rowCounter = 2;

    for (let item of waterItems) {
        console.log('Делаем второй лист')
        for (let subitem of item.subitems) {
            // console.log(item);
            console.log('И так для каждого сабитема');
            console.log(subitem);

            asdf.height(rowCounter, 30);

            asdf.set(1, rowCounter, item.id);
            asdf.set(2, rowCounter, subitem.price);
            asdf.set(3, rowCounter, subitem.podkl);
            asdf.set(4, rowCounter, subitem.furniture);
            asdf.set(5, rowCounter, subitem.razmer);
            asdf.set(6, rowCounter, subitem.type);
            asdf.set(7, rowCounter, subitem.protection);
            asdf.set(8, rowCounter, subitem.color);
            asdf.set(9, rowCounter, item.name);
            asdf.set(10, rowCounter, 'val');
            asdf.set(11, rowCounter, subitem.iddd);
            asdf.set(12, rowCounter, subitem.img);

            rowCounter++;
        }

    }


    await workbook2.save(function(ok) {
        if (!ok)
            workbook2.cancel();
    });

    console.log(
        `
        ⊂_ヽ done
        　 ＼＼
        　　 ＼( ͡° ͜ʖ ͡°)
        　　　 >　⌒ヽ
        　　　/ 　 へ＼
        　　 /　　/　＼＼
        　　 ﾚ　ノ　　 ヽ_つ
        `
    )

    return true;
};

function doRequest(url, json) {
    return new Promise(function(resolve, reject) {
        needle.post(url, json,
            function(err, resp, body) {
                console.log(body);
                resolve(body);
            });
    });
}


router.get('/',
    function(req, res, next) {
        initParse();
        res.json('Начинаем парсить trugor.ru');
    });



module.exports = router;
