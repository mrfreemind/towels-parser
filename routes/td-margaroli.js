let express = require('express');
let router = express.Router();
let config = require('config');
let fs = require('fs');
let request = require('request');
let phantomjs = require('phantom');
let $ = require("jquery");
let promise = require('q');
let system = require('system');
let excelbuilder = require('msexcel-builder');

let id = 4000;

let siteUrl = 'http://www.td-margaroli.ru';
let siteCategories = [
    'http://td-margaroli.ru/index.php?route=product/category&path=63',
    'http://td-margaroli.ru/index.php?route=product/category&path=64',
];
let sitePages = [];
let itemLinks = [];
let items = [];

let sections = [];

let sectionId = 12345;

let dirName = '';


let pageInst;

const download = function(uri, filename, callback) {
    request.head(uri, function(err, res, body) {
        console.log('content-type:', res.headers['content-type']);
        console.log('content-length:', res.headers['content-length']);

        request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
    });
};



const initParse = async() => {
    console.log('initParse');
    let date = new Date();
    dirName = `td-margaroli - ${date.getDate()}.${date.getMonth()}.${date.getFullYear()} - ${date.getHours()}.${date.getMinutes()}.${date.getSeconds()}`;
    console.log('createFolder');
    await fs.mkdirSync(dirName);

    for (let category of siteCategories) {
        console.log(category);
        await parsePagination(category);
    }
    console.log(sitePages);

    await parseCatalog();
    console.log(itemLinks);

    await parseItems();

    await saveXlsx();

};


const parsePagination = async(link) => {
    console.log(`Start parse pagination at ${link}`)

    const instance = await phantomjs.create();
    const page = await instance.createPage();

    const status = await page.open(link);

    let pagesLinks = [];

    let lastPageLink = await page.evaluate(function() {
        return lastPage = $('div.pagination div.links a').last().attr('href');
    });
    let tmpStr = `${link}&page=`;
    let lastId = lastPageLink.slice(tmpStr.length);
    for (let i = 1; i <= lastId; i++) {
        pagesLinks.push(`${tmpStr}${i}`);
    }
    console.log(lastId);
    console.log(lastPageLink);
    console.log(pagesLinks);
    await instance.exit();

    sitePages.push.apply(sitePages, pagesLinks);

    console.log(`End parse pagination at ${link}`);
    return true;
};

const parseCatalog = async() => {
    for (let link of sitePages) {
    // for (let link of [sitePages[0]]) {
        console.log(`Start parse Page ${link}`);

        const instance = await phantomjs.create();
        const page = await instance.createPage();

        const status = await page.open(link);

        let results = await page.evaluate(function() {
            var productCount = 0;
            var arr = [];
            $('ul#product-grid').children('li').each(function(id) {
                productCount++;
                var link = jQuery('ul#product-grid').children('li').eq(id).children('div.container').children('div.name').children('a').attr('href');
                arr.push(link);
            });
            return {
                arr: arr,
                productCount: productCount
            };
        });

        console.log(results.arr);
        console.log('Количество предметов на странице: ' + results.productCount);
        itemLinks.push.apply(itemLinks, results.arr);
        console.log(`Finish parse ${link}`);
        await instance.exit();
    }
    return true;
};

const parseItems = async() => {
    // for (let link of siteCategories) {
    // for (let link of [itemLinks[117]]) {
    // for (let link of ['http://td-margaroli.ru/index.php?route=product/product&path=63&product_id=371']) {
        for (let link of itemLinks) {
        console.log(`Start parse Item ${link}`);

        const instance = await phantomjs.create();
        const page = await instance.createPage();

        const status = await page.open(link);


        await page.injectJs('jquery.min8.js');

        let item = {};
        item.id = id;
        id++;

        let itemSection = await page.evaluate(function() {
            var tmp = {};
            tmp.link = jQuery('div.breadcrumb').children('a').eq(1).attr('href');
            tmp.name = jQuery('div.breadcrumb').children('a').eq(1).text();
            return tmp;
        });
        console.log(itemSection);

        let sectionExist = false;

        let tmpSection;

        for (let section of sections) {
            if (section.name == itemSection.name) {
                sectionExist = true;
                tmpSection = section;
                break;
            }
        }

        item.razd = {};

        if (sectionExist) {
            item.razd = tmpSection;
        } else {
            let sect = {};
            sect.id = sectionId;
            sectionId++;
            sect.name = itemSection.name;
            sections.push(sect);
            item.razd = sect;
        }

        let name = await page.evaluate(function() {
            return jQuery('div.description').children('ul').children('li').last().text();
        })
        item.name = name.slice(8);

        let price = await page.evaluate(function() {
            return jQuery('div.price').children('span').text();
        })
        console.log('prisec: ' + price);
        console.log(price.lastIndexOf('€'));
        console.log(+price.slice(price.lastIndexOf('€') + 1));

        item.price = 78 * +price.slice(price.lastIndexOf('€') + 1);


        let description = await page.evaluate(function() {
            var desc = '';
            jQuery('div.box-content').children('p').each(function (id) {
                desc += jQuery('div.box-content').children('p').eq(id).text() + '\n';
            })
            return desc;
        })

        item.description = description;

        let imgLink = await page.evaluate(function() {
            return jQuery('#image').parent().attr('href');
        })

        item.img = `image_${item.id}.png`;

        download(`${imgLink}`, `./${dirName}/${item.img}`, function() {});
        console.log(item);

        items.push(item);
        await instance.exit();
        console.log(`End parse Item ${link}`);

    }

    return true;
};


router.get('/',
    function(req, res, next) {
        initParse();
        res.json('Начинаем парсить td-margaroli.ru');
    });

const saveXlsx = async() => {
    let workbookImport = await excelbuilder.createWorkbook('./' + dirName + '/', 'import.xlsx');

    let rowCountImport = items.length + 1;

    let importSheet = await workbookImport.createSheet('Товары', 15, rowCountImport);

    importSheet.width(1, 10);
    importSheet.width(2, 30);
    importSheet.width(3, 40);
    importSheet.width(4, 40);
    importSheet.width(5, 40);
    importSheet.width(6, 50);
    importSheet.width(7, 50);
    importSheet.width(8, 50);

    importSheet.set(1, 1, "id");
    importSheet.set(2, 1, "id_razd");
    importSheet.set(3, 1, "razd");
    importSheet.set(4, 1, "name");
    importSheet.set(5, 1, "description");
    importSheet.set(6, 1, "price");
    importSheet.set(7, 1, "img");

    let rowCounter = 2;
    for (let item of items) {
        importSheet.height(rowCounter, 30);

        importSheet.set(1, rowCounter, item.id);
        importSheet.set(2, rowCounter, item.razd.id);
        importSheet.set(3, rowCounter, item.razd.name);
        importSheet.set(4, rowCounter, item.name);
        importSheet.set(5, rowCounter, item.description);
        importSheet.set(6, rowCounter, item.price);
        importSheet.set(7, rowCounter, item.img);

        rowCounter++;
    }


    await workbookImport.save(function(ok) {
        if (!ok)
            workbookImport.cancel();
    });


    console.log(`
                      _______________________________________________________
                      |                                                      |
                 /    |                  ФУРА                                |
                /---, |                   С                                  |
           -----# ==| |                 СУШИЛКАМИ                            |
           | :) # ==| |                                                      |
      -----'----#   | |______________________________________________________|
      |)___()  '#   |______====____   \___________________________________|
     [_/,-,\"--"------ //,-,  ,-,\\\   |/             //,-,  ,-,  ,-,\\ __#
       ( 0 )|===******||( 0 )( 0 )||-  o              '( 0 )( 0 )( 0 )||
    ----'-'--------------'-'--'-'-----------------------'-'--'-'--'-'--------------
            `);

    return true;
};


module.exports = router;
