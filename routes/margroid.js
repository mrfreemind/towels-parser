let express = require('express');
let router = express.Router();
let config = require('config');
let fs = require('fs');
let request = require('request');
let phantomjs = require('phantom');
let $ = require("jquery");
let promise = require('q');
let system = require('system');
let excelbuilder = require('msexcel-builder');

let id = 0;

let siteUrl = 'http://www.margroid.ru';
let siteCategories = [
    'http://www.margroid.ru/shop/folder/elektricheskie',
    'http://www.margroid.ru/shop/folder/vodyanye-1',
    'http://www.margroid.ru/shop/folder/tip_klassika'
];
let sitePages = [];
let siteItemLinks = [];
var items = [];

let dirName = '';


let pageInst;

const download = function(uri, filename, callback){
    request.head(uri, function(err, res, body){
        console.log('content-type:', res.headers['content-type']);
        console.log('content-length:', res.headers['content-length']);

        request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
    });
};


const initParse = async () => {
    let date = new Date();
    dirName = `margroid - ${date.getDate()}.${date.getMonth()}.${date.getFullYear()} - ${date.getHours()}.${date.getMinutes()}.${date.getSeconds()}`;
    await fs.mkdirSync(dirName);
    for (let category of siteCategories) {
        const instance = await phantomjs.create();
        const page = await instance.createPage();

        const status = await page.open(category);
        if (status === 'success') {
            page.injectJs('../jquery.min.js');
            console.log('Загрузили $');
            console.log('Начинаем парсить пагинатор');
            await parsePagination(page, category)
        }
        await instance.exit();
    }
    await parseCatalog();
    await parseItem();
    await saveJson();
    await saveXlsx();
};


const parsePagination = async (page, category) => {
    console.log(`Парсим пагинацию на странице ${category}`);
    let pages_links = await page.evaluate(function (siteUrl) {
        var arr = [];
        $('div.pgn div.pagination ul li a').each(function (id) {
            var tmp = siteUrl + $('div.pagination ul li a').eq(id).attr('href');
            arr.push(tmp);
        });
        return arr;
    }, siteUrl);
    pages_links[0] = category;
    console.log(pages_links);
    console.log(`Закончили парсить пагинацию`);
    sitePages = sitePages.concat(pages_links);
    return true;
};

const parseCatalog = async () => {
    for (let link of sitePages) {
        await parsePage(link);
    }
    return true;
};


const parsePage = async (link) => {
    console.log(`Start parsePage ${link}`);

    const instance = await phantomjs.create();
    const page = await instance.createPage();

    const status = await page.open(link);
    console.log(status);

    let results = await page.evaluate(function (siteUrl) {
        var ctl = 'div.catalog-line';
        var ctlCount = 0;
        var productCount = 0;
        var arr = [];
        $(ctl).each(function (id) {
            ctlCount++;
            $(ctl).eq(id).children('.product').each(function (iId) {
                productCount++;
                arr.push(siteUrl + $(ctl).eq(id).children('.product').eq(iId).children('form').children('a').attr('href'))
            })
        });
        return {arr: arr, ctlCount: ctlCount, productCount: productCount};
    }, siteUrl);
    siteItemLinks = siteItemLinks.concat(results.arr);
    console.log('Количество предметов на странице: ' + results.productCount);
    await instance.exit();
};


const parseItem = async () => {
    for (let itemLink of siteItemLinks) {

        let instance = await phantomjs.create(['--load-images=no']);
        let page = await instance.createPage();


        let status = await page.open(itemLink);

        let item = {};
        console.log(status);
        console.log('Зашли на страницу: ' + itemLink);
        console.time('Начинаем считать время');

        item.id = Number(id);
        console.log('Записали ID: ' + item.id);

        item.category = await page.evaluate(function () {
            return $('.breadcrumbs').children('a').eq(2).text();
        });
        console.log('Записали категорию: ' + item.category);

        item.subCategory = await page.evaluate(function () {
            if ($('.breadcrumbs').children('a').eq(3).text()) {
                return $('.breadcrumbs').children('a').eq(3).text();
            }
            return '';
        });
        console.log('Записали подкатегорию: ' + item.subCategory);

        item.name = await page.evaluate(function () {
            return $('article.main h1').text();
        });
        console.log('Записали имя: ' + item.name);

        item.char = await page.evaluate(function () {
            var text = '';
            if ($('div.char')) {
                $('div.char').children('ul').children('li').each(function (id) {
                    text += $('div.char').children('ul').children('li').eq(id).text();
                });
                text += '\n';
            }
            if ($('div.compl')) {
                $('div.compl').children('ul').children('li').each(function (id) {
                    text += $('div.compl').children('ul').children('li').eq(id).text();
                });
            }
            return text;

        });
        console.log('Записали характеристики и комплектацию:' + item.char);

        let img_link = await page.evaluate(function (item, siteUrl) {
            var img = '';
            if ($('div.big-pic')) {
                img = siteUrl + $('div.big-pic').children('a').children('img').attr('src');
                item.img = 'img_' + item.id + '.png';
            }
            return img;

        }, item, siteUrl);

        if (img_link) {
            item.img = 'img_' + item.id + '.png';
            await download(img_link, './' + dirName + '/' + item.img, function(){
                console.log('Сохранили изображение');
            });
            console.log('Сохранили название картинки:' + item.img);
        }


        item.description = await page.evaluate(function () {
            var text = '';
            $('div.info-text').children('p').each(function (id) {
                text += $('div.info-text').children('p').eq(id).text();
            });
            $('div.info-text').children('div').eq(0).each(function (id) {
                text += $('div.info-text').children('div').eq(0).children('p').eq(id).text();
            });
            return text;
        });
        console.log('Записали описание: ' + item.description);

        item.subItems = await page.evaluate(function (item) {
            var arr = [];
            $('select#o_product_size option').each(function (id) {
                var _item = {};
                _item.parentId = item.id;
                var obj = $('select#o_product_size option').eq(id);
                if (obj.text().trim()) {
                    _item.cost = obj.attr('data-price');

                    var specs = obj.text().split(' /');
                    if (specs[0]) {
                        _item.size = specs[0].trim();
                    }
                    if (specs[1]) {
                        _item.insertions = specs[1].trim();
                    }
                    if (specs[2]) {
                        _item.heatTransfer = specs[2].trim();
                    }
                    arr.push(_item);
                }
            });
            return arr;
        }, item);
        console.log('Записали варианты');
        console.timeEnd('Начинаем считать время');
        console.log(item);
        items.push(item);
        id++;
        await instance.exit();
    }
};


const saveJson = async () => {
    const json = JSON.stringify(items);
    await fs.writeFile("./" + dirName + "/output.json", json, 'utf8', function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("JSON сохранен");
    });
    return true;
};

const saveXlsx = async () => {
    let workbook = await excelbuilder.createWorkbook('./' + dirName + '/', 'output.xlsx');

    let rowCount = 1;
    for (let item of items) {
        rowCount++;
        rowCount += item.subItems.length;
    }

    let sheet = await workbook.createSheet('Лист1', 15, rowCount);

    // Настройки ширины столбцов
    sheet.width(1, 10);
    sheet.width(2, 30);
    sheet.width(3, 40);
    sheet.width(4, 40);
    sheet.width(5, 40);
    sheet.width(6, 50);
    sheet.width(7, 50);
    sheet.width(8, 15);
    sheet.width(9, 15);
    sheet.width(10, 15);
    sheet.width(11, 15);
    sheet.width(12, 15);

    // Шапка файла
    sheet.set(1, 1, "id");
    sheet.set(2, 1, "Связь с товаром (для товарного предложения)");
    sheet.set(3, 1, "Раздел");
    sheet.set(4, 1, "Подраздел");
    sheet.set(5, 1, "Наименование товара");
    sheet.set(6, 1, "Характеристики и комплектация");
    sheet.set(7, 1, "Описание");
    sheet.set(8, 1, "Изображения");
    sheet.set(9, 1, "Цена");
    sheet.set(10, 1, "Размер");
    sheet.set(11, 1, "Вставки");
    sheet.set(12, 1, "Теплоотдача");

    let rowCounter = 2;
    for (let item of items) {
        sheet.height(rowCounter, 30);

        sheet.set(1, rowCounter, item.id);
        sheet.set(3, rowCounter, item.category);
        sheet.set(4, rowCounter, item.subCategory);
        sheet.set(5, rowCounter, item.name);
        sheet.set(6, rowCounter, item.char);
        sheet.set(7, rowCounter, item.description);
        sheet.set(8, rowCounter, item.img);

        rowCounter++;

        for (let subItem of item.subItems) {
            sheet.set(2, rowCounter, subItem.parentId);
            sheet.set(9, rowCounter, subItem.cost);
            sheet.set(10, rowCounter, subItem.size);
            sheet.set(11, rowCounter, subItem.insertions);
            sheet.set(12, rowCounter, subItem.heatTransfer);

            rowCounter++;
        }
    }


    await workbook.save(function(ok) {
        if (!ok)
            workbook.cancel();
    });

    console.log(
        `
        ⊂_ヽ done
        　 ＼＼
        　　 ＼( ͡° ͜ʖ ͡°)
        　　　 >　⌒ヽ
        　　　/ 　 へ＼
        　　 /　　/　＼＼
        　　 ﾚ　ノ　　 ヽ_つ
        `
    )

    return true;
};


router.get('/',
    function(req, res, next){
        initParse();
        res.json('Начинаем парсить margroid.ru');
});



module.exports = router;
