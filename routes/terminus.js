let express = require('express');
let router = express.Router();
let config = require('config');
let fs = require('fs');
let request = require('request');
let needle = require('needle');
let phantomjs = require('phantom');
let $ = require("jquery");
let promise = require('q');
let system = require('system');
let excelbuilder = require('msexcel-builder');

let dirName = '';

let pageInst;

let id = 1500;

let subItemId = 50000;

let razd_1 = [];
let razd_2 = [];

let razd_1_id = 2000;
let razd_2_id = 20000;

let siteUrl = 'http://www.terminus.ru';
let siteCategories = [
    'http://terminus.ru/catalog/electro/',
    'http://terminus.ru/catalog/water/'
]
let itemLinks = [];
let items = [];


const initParse = async() => {
    console.log('initParse');
    let date = new Date();
    dirName = `terminus - ${date.getDate()}.${date.getMonth()}.${date.getFullYear()} - ${date.getHours()}.${date.getMinutes()}.${date.getSeconds()}`;
    await fs.mkdirSync(dirName);
    console.log('init done');
    await parseCatalog();
    saveJson(itemLinks, 'itemLinks');
    await parseItems();
    await saveJson(razd_1, 'razd_1');
    await saveJson(razd_2, 'razd_2');
    await saveXlsx();

};

const parseCatalog = async() => {
    for (let link of siteCategories) {
        console.log(`Start parsePage ${link}`);

        const instance = await phantomjs.create();
        const page = await instance.createPage();

        const status = await page.open(link);
        console.log(status);

        let results = await page.evaluate(function(siteUrl) {
            var productCount = 0;
            var arr = [];
            jQuery('section.product-block').children('section.product-item').each(function(id) {
                productCount++;
                arr.push(siteUrl +
                    jQuery('section.product-block').children('section.product-item').eq(id).children('form').children('a.img').attr('href')
                );
            });
            return {
                arr: arr,
                productCount: productCount
            };
        }, siteUrl);
        console.log(results.arr);
        console.log('Количество предметов на странице: ' + results.productCount);
        itemLinks.push.apply(itemLinks, results.arr);
        await instance.exit();

    }

    return true;
};


const parseItems = async() => {
    for (let itemLink of itemLinks) {
        let instance = await phantomjs.create(['--load-images=no']);
        let page = await instance.createPage();

        let status = await page.open(itemLink);
        console.log('Зашли на страницу: ' + itemLink);

        let item = {};
        item.id = Number(id);
        console.log('Записали ID: ' + item.id);

        item.url = itemLink;

        let crumbs = await page.evaluate(function() {
            var result = {};
            var arr = [];
            result.amount = jQuery('ul.bc-nav').children('li').length;
            jQuery('ul.bc-nav').children('li').each(function(id) {
                var tmp = {};
                if (jQuery('ul.bc-nav').children('li').eq(id).children('a').length > 0) {
                    tmp.name =
                        jQuery('ul.bc-nav').children('li').eq(id)
                        .children('a').text();
                    tmp.url =
                        jQuery('ul.bc-nav').children('li').eq(id)
                        .children('a').attr('href');
                    arr.push(tmp);
                } else {
                    tmp.name = jQuery('ul.bc-nav').children('li').eq(id).text();
                    tmp.url = '';
                    arr.push(tmp);
                }

            });
            result.arr = arr;
            return result;
        });

        let imgLink = await page.evaluate(function () {
            var imgLink = '';
            jQuery('section.main-image').children('img').each(function(id) {
                if (jQuery('section.main-image').children('img').eq(id).css('display').toLowerCase() == 'inline') {
                    imgLink = jQuery('section.main-image').children('img').eq(id).attr('src');
                    return false;
                }
            });
            return imgLink;
        })

        item.img = `img_${item.id}.png`;
        await download(`${siteUrl}${imgLink}`, `./${dirName}/${item.img}`, function() {});

        console.log(crumbs);

        let _razd_1 = {};
        let _razd_2 = {};

        switch (crumbs.amount) {
            case 5:
                item.name = crumbs.arr[4].name;
                _razd_1 = crumbs.arr[2];
                _razd_2 = crumbs.arr[3];

                // Главный раздел
                _razd_1_b = false;
                for (let razd of razd_1) {
                    if (razd.name == _razd_1.name) {
                        item.razd_1 = razd;
                        _razd_1_b = true;
                        break;
                    }
                };

                if (!_razd_1_b) {
                    razd_1.push({
                        id: razd_1_id,
                        name: _razd_1.name,
                        url: _razd_1.url,
                    });
                    razd_1_id++;
                    item.razd_1 = razd_1[razd_1.length - 1];
                };

                // Подкаталог 1 уровня
                _razd_2_b = false;
                for (let razd of razd_2) {
                    if (razd.name == _razd_2.name) {
                        item.razd_2 = razd;
                        _razd_2_b = true;
                        break;
                    }
                };

                if (!_razd_2_b) {
                    razd_2.push({
                        id: razd_2_id,
                        name: _razd_2.name,
                        url: _razd_2.url,
                    });
                    razd_2_id++;
                    item.razd_2 = razd_2[razd_2.length - 1];
                };
                break;
            case 4:
                item.name = crumbs.arr[3].name;
                _razd_1 = crumbs.arr[2];

                // Главный раздел
                _razd_1_b = false;
                for (let razd of razd_1) {
                    if (razd.name == _razd_1.name) {
                        item.razd_1 = razd;
                        _razd_1_b = true;
                        break;
                    }
                };

                if (!_razd_1_b) {
                    razd_1.push({
                        id: razd_1_id,
                        name: _razd_1.name,
                        url: _razd_1.url,
                    });
                    razd_1_id++;
                    item.razd_1 = razd_1[razd_1.length - 1];
                };

                item.razd_2 = _razd_2;

                // Подкаталог 2 уровня
                item.razd_2.id = '';
                item.razd_2.name = '';
                item.razd_2.url = '';
                break;
        }

        let subitems = await page.evaluate(function() {
            var arr = [];
            jQuery('select[name="mprice"]').children('option').each(function(id) {
                var subitem = {};
                var value = jQuery('select[name="mprice"]').children('option').eq(id).val();
                jQuery('select[name="mprice"]').val(value).change();
                var size = jQuery('select[name="mprice"]').children('option').eq(id).text();
                var price = jQuery('div.price-b span').text();
                var char = '';
                jQuery('section.tech-desc-block').eq(0).children('div').each(function(id) {
                    char += jQuery('section.tech-desc-block').eq(0).children('div').eq(id).text() + '\n';
                });
                var kit = '';
                jQuery('section.tech-desc-block').eq(1).children('div').each(function(id) {
                    kit += jQuery('section.tech-desc-block').eq(1).children('div').eq(id).text() + '\n';
                });
                var imgLink = '';
                jQuery('section.main-image').children('img').each(function(id) {
                    if (jQuery('section.main-image').children('img').eq(id).css('display').toLowerCase() == 'inline') {
                        imgLink = jQuery('section.main-image').children('img').eq(id).attr('src');
                        return false;
                    }
                });
                subitem.size = size;
                subitem.price = price;
                subitem.char = char;
                subitem.kit = kit;
                subitem.imgLink = imgLink;
                arr.push(subitem);
            })
            return arr;
        });

        let description = await page.evaluate(function() {
            return jQuery('section.desc-text').eq(0).text();
        });

        item.description = description;

        item.subitems = subitems;

        for (let subitem of item.subitems) {
            subitem.iddd = subItemId;
            subItemId++;
            if (subitem.imgLink != '') {
                let imgName = '';
                console.log(subitem.imgLink);
                subitem.img = `img_${item.id}_${subitem.iddd}.png`;
                await download(`${siteUrl}${subitem.imgLink}`, `./${dirName}/${subitem.img}`, function() {});
            }
        }

        console.log(item);
        items.push(item);
        saveJson(item, 'item_' + item.id);
        id++;
        await instance.exit();
    }
};

router.get('/',
    function(req, res, next) {
        initParse();
        res.json('Начинаем парсить terminus.ru');
    });

const saveXlsx = async() => {
    let workbookImport = await excelbuilder.createWorkbook('./' + dirName + '/', 'import.xlsx');
    let workbookOffers = await excelbuilder.createWorkbook('./' + dirName + '/', 'offers.xlsx');

    let rowCountImport = items.length + 1;
    let rowCountOffers = 1;
    for (let item of items) {
        rowCountOffers += item.subitems.length;
    }

    let importSheet = await workbookImport.createSheet('Товары', 15, rowCountImport);
    let offersSheet = await workbookOffers.createSheet('Товарные предложения', 15, rowCountOffers);

    importSheet.width(1, 10);
    importSheet.width(2, 30);
    importSheet.width(3, 40);
    importSheet.width(4, 40);
    importSheet.width(5, 40);
    importSheet.width(6, 50);
    importSheet.width(7, 50);
    importSheet.width(8, 50);

    importSheet.set(1, 1, "id");
    importSheet.set(2, 1, "id_razd");
    importSheet.set(3, 1, "razd");
    importSheet.set(4, 1, "id_razd_2");
    importSheet.set(5, 1, "razd_2");
    importSheet.set(6, 1, "name");
    importSheet.set(7, 1, "description");
    importSheet.set(7, 1, "img");

    let rowCounter = 2;
    for (let item of items) {
        importSheet.height(rowCounter, 30);

        importSheet.set(1, rowCounter, item.id);
        importSheet.set(2, rowCounter, item.razd_1.id);
        importSheet.set(3, rowCounter, item.razd_1.name);
        importSheet.set(4, rowCounter, item.razd_2.id);
        importSheet.set(5, rowCounter, item.razd_2.name);
        importSheet.set(6, rowCounter, item.name);
        importSheet.set(7, rowCounter, item.description);
        importSheet.set(7, rowCounter, item.img);

        rowCounter++;
    }


    await workbookImport.save(function(ok) {
        if (!ok)
            workbookImport.cancel();
    });

    offersSheet.width(1, 10);
    offersSheet.width(2, 30);
    offersSheet.width(3, 40);
    offersSheet.width(4, 40);
    offersSheet.width(5, 40);
    offersSheet.width(6, 50);
    offersSheet.width(7, 50);
    offersSheet.width(8, 15);
    offersSheet.width(9, 15);

    offersSheet.set(1, 1, "id");
    offersSheet.set(2, 1, "price");
    offersSheet.set(3, 1, "razmer");
    offersSheet.set(4, 1, "char");
    offersSheet.set(5, 1, "kit");
    offersSheet.set(6, 1, "img");
    offersSheet.set(7, 1, "name");
    offersSheet.set(8, 1, "val");
    offersSheet.set(9, 1, "Iddd");

    rowCounter = 2;

    for (let item of items) {
        for (let subitem of item.subitems) {
            offersSheet.height(rowCounter, 30);

            offersSheet.set(1, rowCounter, item.id);
            offersSheet.set(2, rowCounter, subitem.price);
            offersSheet.set(3, rowCounter, subitem.size);
            offersSheet.set(4, rowCounter, subitem.char);
            offersSheet.set(5, rowCounter, subitem.kit);
            offersSheet.set(6, rowCounter, subitem.img);
            offersSheet.set(7, rowCounter, item.name);
            offersSheet.set(8, rowCounter, 'val');
            offersSheet.set(9, rowCounter, subitem.iddd);

            rowCounter++;
        }
    }

    await workbookOffers.save(function(ok) {
        if (!ok)
            workbookOffers.cancel();
    });

    console.log(`
                  _______________________________________________________
                  |                                                      |
             /    |                  ФУРА                                |
            /---, |                   С                                  |
       -----# ==| |                 СУШИЛКАМИ                            |
       | :) # ==| |                                                      |
  -----'----#   | |______________________________________________________|
  |)___()  '#   |______====____   \___________________________________|
 [_/,-,\"--"------ //,-,  ,-,\\\   |/             //,-,  ,-,  ,-,\\ __#
   ( 0 )|===******||( 0 )( 0 )||-  o              '( 0 )( 0 )( 0 )||
----'-'--------------'-'--'-'-----------------------'-'--'-'--'-'--------------
        `);

    return true;
};


const download = function(uri, filename, callback) {
    request.head(uri, function(err, res, body) {
        // console.log('content-type:', res.headers['content-type']);
        // console.log('content-length:', res.headers['content-length']);
        request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
    });
};

const saveJson = async(arr, name) => {
    const json = JSON.stringify(arr);
    await fs.writeFile("./" + dirName + "/" + name + ".json", json, 'utf8', function(err) {
        if (err) {
            return console.log(err);
        }
        console.log(`JSON ${name} сохранен`);
    });
    return true;
};


module.exports = router;
