var express = require('express');
var path = require('path');
var favicon = require('serve-favicon')
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');


var margroid = require('./routes/margroid');
var td_margaroli = require('./routes/td-margaroli');
var trugor = require('./routes/trugor');
var terminus = require('./routes/terminus');
var sunerzha = require('./routes/sunerzha');


var app = express();

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());


app.use('/margroid', margroid);
app.use('/td-margaroli', td_margaroli);
app.use('/trugor', trugor);
app.use('/terminus', terminus);
app.use('/sunerzha', sunerzha);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500).send('error', {error: err});
});

module.exports = app;
